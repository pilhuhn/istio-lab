
if [ `oc whoami` != 'kube:admin' ]
then
 echo "Must run as kubeadmin";
 exit 1;
fi

for i in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15
#for i in 06 07 08 09 10
do
 	luser=user$i
	lproj=project-$i

	oc create clusterrolebinding $luser-binding --clusterrole=basic-user --user=$luser
	oc new-project $lproj
	oc adm policy add-role-to-user admin $luser -n $lproj
	oc adm policy add-scc-to-user anyuid -z default -n $lproj
	oc adm policy add-scc-to-user privileged -z default -n $lproj
	oc label namespace $lproj istio-injection=enabled

	oc create -n $lproj -f https://raw.githubusercontent.com/thoraxe/istio-lab-summit-2019/master/src/deployments/curl.yaml
	oc create -n $lproj -f https://raw.githubusercontent.com/thoraxe/istio-lab-summit-2019/master/src/deployments/customer.yaml
#	oc create -n $lproj -f https://raw.githubusercontent.com/thoraxe/istio-lab-summit-2019/master/src/deployments/gateway.yaml
	oc create -n $lproj -f https://raw.githubusercontent.com/thoraxe/istio-lab-summit-2019/master/src/deployments/preference.yaml
	oc create -n $lproj -f https://raw.githubusercontent.com/thoraxe/istio-lab-summit-2019/master/src/deployments/recommendation.yaml

done


#oc create -n istio-system -f gateway.yaml
#oc create -n istio-system -f globalgateway.yaml

